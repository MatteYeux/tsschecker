CC = gcc
uname_s = $(shell uname -s)
TARGET = tsschecker
LD_FLAGS = -I /usr/local/include -lcurl -lbsd -lm -lplist -L /usr/local/lib/ -lpartialzip-1.0
all : $(TARGET)

$(TARGET) : src/main.o src/download.o src/jsmn.o src/tss.o src/tsschecker.o
		$(CC) src/main.o src/download.o src/jsmn.o src/tss.o src/tsschecker.o $(LD_FLAGS) -o $(TARGET)
		@echo "Successfully built $(TARGET) for $(uname_s)"

main.o : src/main.c
		$(CC) -c src/main.c -o src/main.o

download.o : src/download.c 
		$(CC) -c src/download.c -o src/download.o

jsmn.o : src/jsmn.c
		$(CC) -c src/jsmn.c -o src/jsmn.o

tss.o : src/tss.c
		$(CC) -c src/tss.c -o src/tss.o

tsschecker.o :
		$(CC) -c src/tsschecker.c -o src/tsschecker.o

install :
		cp $(TARGET) /usr/local/bin/
		@echo "Installed $(TARGET)"
clean :
		rm -rf src/*.o $(TARGET)	